# coding: utf-8
#This is the main file. Run this, to run the program
#import antigravity
import random
import pygame, sys
import os
from pygame.locals import *
import math

import Rooms
import Menu

#Setup
pygame.init()

white = (255,255,255)
# set up fonts
basicFont = pygame.font.SysFont(None, 48)

FPS = 300 # 60 frames per second
clock = pygame.time.Clock()

#window
SWIDTH = 240
SHEIGTH = 240
global MainWindow
MainWindow = pygame.display.set_mode((SWIDTH, SHEIGTH))
def game():
	print "new game"
	done = False
	global room
	room = Rooms.blank_room()
	while not done:
		for event in pygame.event.get(): # User did something
			if event.type == pygame.QUIT: # Closed from X
				done = True # Stop the Loop
			if event.type == pygame.MOUSEBUTTONUP:
				cor = pygame.mouse.get_pos()
				room[cor[0]/30][cor[1]/30].click()
		MainWindow.fill(white)

		for col in room:
			for tile in col:
				tile.draw()
		pygame.display.flip()
		clock.tick(FPS)
		pygame.display.set_caption("FPS: %i" % clock.get_fps())
		
def end():
	won = True
	for col in room:
		for tile in col:
			if not tile.sta == 2:
				won = False
	if won:
		print "You won the game. Congratulations on your effort"
	else:
		print "You lost. try again?"
		done = False
		buttons = []
		buttons.append(Menu.Button(40,40,160,40,game,"New Game"))
		buttons.append(Menu.Button(40,100,160,40,pygame.quit,"Quit"))
		while not done:
			for event in pygame.event.get(): # User did something
				if event.type == pygame.QUIT: # Closed from X
					done = True # Stop the Loop
				if event.type == pygame.MOUSEBUTTONUP:
					for button in buttons:
						done = button.pressed(pygame.mouse.get_pos())
						if done:
							button.funk()
							break
			for button in buttons:
				button.draw()
				pygame.display.flip()
game()
pygame.quit()
