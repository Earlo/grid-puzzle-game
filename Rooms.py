import pygame
import random
black 	= [0	,0		,0]
red		= (255	,0		,0)
green	= (0	,255	,0)
yellow	= (255	,255	,0)
blue	= (0	,0		,255)
pink	= (255	,0		,255)
cyan	= (0	,255	,255)
white 	= [255	,255	,255]
class Tile (object):
	def __init__(self,x,y,ocp):
		self.pos=[x,y]
		self.rect = pygame.Rect(x*30,y*30,30,30)
		self.sta = ocp
		self.colour = [0,0,0]
		self.line = 1
		self.movcost = 1
		self.next = []	# nodes next to this one go in heres
		
	def click(self):
		from Main import room
		if self.sta == 1:
			x = self.pos[0]
			y = self.pos[1]
			for col in room:
				for tile in col:
					if not (tile.sta == 2):
						tile.sta = 0
						
			end = True
			print self.pos
			self.sta = 2
			if x >= 3:
				if not (room[x-3][y].sta == 2):
					room[x-3][y].sta = 1
					end = False
			if x <= 4:
				if not (room[x+3][y].sta == 2):
					room[x+3][y].sta = 1
					end = False
			if y >= 3:
				if not (room[x][y-3].sta == 2):
					room[x][y-3].sta = 1
					end = False
			if y <= 4:
				if not (room[x][y+3].sta == 2):
					room[x][y+3].sta = 1
					end = False
			if x >=2 and y >=2:
				if not (room[x-2][y-2].sta == 2):
					room[x-2][y-2].sta = 1
					end = False
			if x >=2 and y <=5:
				if not (room[x-2][y+2].sta  == 2):
					room[x-2][y+2].sta = 1
					end = False
			if x <=5 and y >=2:
				if not (room[x+2][y-2].sta  == 2):
					room[x+2][y-2].sta = 1
					end = False
			if x <=5 and y <=5:
				if not (room[x+2][y+2].sta == 2):
					room[x+2][y+2].sta = 1
					end = False
			if end:
				print "Game over"
				from Main import end
				end()
			
	def draw(self):
		if (self.sta == 0):
			self.line = 1
			self.colour = [0,0,0]
		elif (self.sta == 1):
			self.line = 3
			self.colour = [255,40,0]
		elif (self.sta == 2):
			self.line = 0
			self.colour = [0,0,0]
		from Main import MainWindow
		pygame.draw.rect(MainWindow,self.colour,self.rect,self.line)
		
#~~~~~~~~~~~~~~~~~~~~~~~~~Tile end~~~~~~~~~~~~~~~~~~~~~


#room blueprints

def build_room(blueprint,size):
	room = []
	for x in range(0,size[0]):
		room.append([])
		for y in range(0,size[1]):
			room[x].append(Tile(x,y,blueprint[size[0]*y+x]))
	return room
	
def blank_room():
	size = [8,8]
	blueprint =[1,1,1,1,1,1,1,1,
				1,1,1,1,1,1,1,1,
				1,1,1,1,1,1,1,1,
				1,1,1,1,1,1,1,1,
				1,1,1,1,1,1,1,1,
				1,1,1,1,1,1,1,1,
				1,1,1,1,1,1,1,1,
				1,1,1,1,1,1,1,1]
	return build_room(blueprint,size)
