# -*- coding: cp1252 -*-
import pygame, sys
from pygame.locals import *

black 	= (0	,0		,0)
red		= (255	,0		,0)
green	= (0	,255	,0)
yellow	= (255	,255	,0)
blue	= (0	,0		,255)
pink	= (255	,0		,255)
cyan	= (0	,255	,255)
white 	= (255	,255	,255)
pygame.font.init()
class Button (object):	#Menu Button
	def __init__(self,x,y,w,l,funk,text):
		self.pos = [x,y]
		self.rect = pygame.Rect(x,y,w,l)
		self.text = text
		self.font = pygame.font.SysFont("Calibri", 15)
		self.label = self.font.render(self.text, 1, black)
		self.funk = funk
	def pressed(self, mouse):
		if self.rect.collidepoint(mouse):
			return True
		else: return False	
	def draw(self):
		from Main import MainWindow
		pygame.draw.rect(MainWindow, blue,self.rect,0)
		MainWindow.blit(self.label, (self.pos[0],self.pos[1]))

		
class Obutton (object):
	def __init__(self,x,y,w,l):
		self.pos =(x,y)
		self.width = w
		self.length = l
		self.text = "Options"
		self.font = pygame.font.SysFont("Calibri", 15)
		self.label = self.font.render(self.text, 1, black)
		
	def pressed(self, mouse):
		if mouse[0] > self.pos[0]:
			if mouse[1] > self.pos[1]:
				if mouse[0] < self.pos[0] + self.width:
					if mouse[1] < self.pos[1] + self.length:
						print "Options button was pressed!"
						return True
					else: return False
				else: return False
			else: return False
		else: return False
	
	def draw(self):
		from Main import MainWindow
		pygame.draw.rect(MainWindow, blue, (self.pos[0],self.pos[1],self.width,self.length),0)
		MainWindow.blit(self.label, (self.pos))